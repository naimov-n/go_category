package category

import (
	"category/internal/entity"
	"category/internal/service/category"
	"context"
	"github.com/uptrace/bun"
)

type Repository struct {
	db *bun.DB
}

func (r Repository) Create(ctx context.Context, data category.Create) error {
	var detail entity.Category

	detail.Title = data.Title
	detail.Status = true

	_, err := r.db.NewInsert().Model(&detail).Exec(ctx)

	if err != nil {
		return err
	}

	return nil
}

func (r Repository) GetAll(ctx context.Context, limit, offset int) (int, []entity.Category, error) {
	var lists []entity.Category

	q := r.db.NewSelect().Model(&lists).Where("status = true")

	if limit > 0 {
		q.Limit(limit)
	}

	if offset > 0 {
		q.Offset(offset)
	}

	count, err := q.ScanAndCount(ctx)

	if err != nil {
		count = 0
	}

	return count, lists, nil
}

func (r Repository) GetById(ctx context.Context, id int) (entity.Category, error) {
	//TODO implement me
	panic("implement me")
}

func (r Repository) Update(ctx context.Context, data category.Update) error {
	//TODO implement me
	panic("implement me")
}

func (r Repository) Delete(ctx context.Context, id int) error {
	//TODO implement me
	panic("implement me")
}

func NewRepository(db *bun.DB) *Repository {
	return &Repository{db: db}
}
