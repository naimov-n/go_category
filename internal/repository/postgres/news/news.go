package news

import (
	"category/internal/entity"
	"category/internal/service/news"
	"context"
	"github.com/uptrace/bun"
)

type Repository struct {
	db *bun.DB
}

func NewRepository(db *bun.DB) *Repository {
	return &Repository{db: db}
}

func (r Repository) Create(ctx context.Context, data news.Create) error {
	var detail entity.NewsPost

	detail.Title = data.Title
	detail.Content = data.Content
	detail.CategoryId = data.CategoryId

	_, err := r.db.NewInsert().Model(&detail).Exec(ctx)

	if err != nil {
		return err
	}
	return nil
}

func (r Repository) GetAll(ctx context.Context, limit, offset, categoryId int) (int, []entity.NewsPost, error) {
	var lists []entity.NewsPost
	q := r.db.NewSelect().Model(&lists).Where("status = true")

	if categoryId > 0 {
		q.Where("category_id = ?", categoryId)
	}

	if limit > 0 {
		q.Limit(limit)
	}
	if offset > 0 {
		q.Offset(offset)
	}

	count, err := q.ScanAndCount(ctx)

	if err != nil {
		count = 0
	}

	return count, lists, nil
}

func (r Repository) GetById(ctx context.Context, id int) (entity.NewsPost, error) {
	var list entity.NewsPost

	err := r.db.NewSelect().Model(&list).Where("id = ?", id).Scan(ctx)

	if err != nil {
		return entity.NewsPost{}, err
	}

	return list, nil
}

func (r Repository) Update(ctx context.Context, id int, data news.Update) error {
	var detail entity.NewsPost

	detail.Title = data.Title
	detail.Content = data.Content
	detail.CategoryId = data.CategoryId
	detail.Status = true

	_, err := r.db.NewUpdate().Model(&detail).Where("id = ?", id).Exec(ctx)

	if err != nil {
		return err
	}

	return nil
}

func (r Repository) Delete(ctx context.Context, id int) error {
	_, err := r.db.NewDelete().Table("news").Where("id = ?", id).Exec(ctx)

	if err != nil {
		return err
	}

	return nil

}
