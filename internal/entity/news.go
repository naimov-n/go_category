package entity

import "github.com/uptrace/bun"

type NewsPost struct {
	bun.BaseModel `bun:"table:news"`

	Id         int    `json:"id"          bun:"id,pk,autoincrement"`
	CategoryId int    `json:"categoryId"  bun:"category_id"`
	Title      string `json:"title"       bun:"title"`
	Content    string `json:"content"     bun:"content"`
	Status     bool   `json:"status"      bun:"status"`
}
