package entity

import "github.com/uptrace/bun"

type Category struct {
	bun.BaseModel `bun:"table: category"`
	Id            int    `json:"id"     bun:"id,pk,autoincrement"`
	Title         string `json:"title"  bun:"title"`
	Status        bool   `json:"status" bun:"status"`
}
