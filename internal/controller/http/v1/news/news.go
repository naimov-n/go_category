package news

import (
	news_service "category/internal/service/news"
	"category/internal/usecase/news"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type Controller struct {
	useCase *news.UseCase
}

func NewController(useCase *news.UseCase) *Controller {
	return &Controller{useCase: useCase}
}

func (uc Controller) Create(c *gin.Context) {
	var data news_service.Create

	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "error data",
			"status":  false,
		})

		return
	}

	ctx := context.Background()

	err := uc.useCase.Create(ctx, data)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "Couldn't create news",
			"status":  false,
			"error":   err.Error(),
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Ok",
		"status":  true,
	})

}

func (uc Controller) GetAll(c *gin.Context) {

	q := c.Request.URL.Query()
	var limit, offset int
	ctx := context.Background()

	p := q["categoryId"]
	categoryId := 0
	if len(p) > 0 {
		categoryIdInt, err := strconv.Atoi(p[0])

		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"message": "invalid params",
				"status":  false,
			})
		}
		categoryId = categoryIdInt
	}

	limitQ := q["limit"]
	if len(limitQ) > 0 {
		limitInt, err := strconv.Atoi(limitQ[0])
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"message": "invalid limit",
				"status":  false,
			})

			return
		}

		limit = limitInt
	}
	offsetQ := q["offset"]
	if len(offsetQ) > 0 {
		offsetInt, err := strconv.Atoi(offsetQ[0])
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"message": "invalid offset",
				"status":  false,
			})

			return
		}

		offset = offsetInt
	}

	count, data, err := uc.useCase.GetAll(ctx, limit, offset, categoryId)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid data",
			"error":   err.Error(),
			"status":  false,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,

		"data": map[string]interface{}{
			"result": data,
			"count":  count,
		},
	})
}

func (uc Controller) GetById(c *gin.Context) {
	idInt := c.Param("id")

	id, err := strconv.Atoi(idInt)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid id",
			"status":  false,
		})
		return
	}

	ctx := context.Background()

	data, err := uc.useCase.GetById(ctx, id)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "not found",
			"error":   err.Error(),
			"status":  false,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,
		"data": map[string]interface{}{
			"result": data,
		},
	})

}

func (uc Controller) Update(c *gin.Context) {
	idInt := c.Param("id")

	id, err := strconv.Atoi(idInt)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid id",
			"status":  false,
		})
		return
	}

	var data news_service.Update

	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid data",
			"status":  false,
		})
		return
	}

	ctx := context.Background()

	err = uc.useCase.Update(ctx, id, data)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "error update data",
			"status":  false,
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,
	})
}

func (uc Controller) Delete(c *gin.Context) {
	idInt := c.Param("id")

	id, err := strconv.Atoi(idInt)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid param",
			"status":  false,
		})
		return
	}

	ctx := context.Background()

	err = uc.useCase.Delete(ctx, id)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "Error on delete",
			"status":  false,
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,
	})
}
