package category

import (
	category2 "category/internal/service/category"
	"category/internal/usecase/category"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type Controller struct {
	useCase *category.UseCase
}

func (uc Controller) Create(c *gin.Context) {
	var data category2.Create

	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "invalid data",
			"status":  false,
		})

		return
	}

	ctx := context.Background()

	err := uc.useCase.Create(ctx, data)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "error create",
			"error":   err.Error(),
			"status":  false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,
	})
}

func (uc Controller) GetAll(c *gin.Context) {
	q := c.Request.URL.Query()

	var limit, offset int

	limitQ := q["limit"]

	if len(limitQ) > 0 {
		limitInt, err := strconv.Atoi(limitQ[0])

		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"message": "invalid limit",
				"status":  false,
			})
			return
		}

		limit = limitInt
	}
	offsetQ := q["limit"]

	if len(offsetQ) > 0 {
		offsetInt, err := strconv.Atoi(offsetQ[0])

		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"message": "invalid limit",
				"status":  false,
			})
			return
		}

		limit = offsetInt
	}

	ctx := context.Background()

	data, err := uc.useCase.GetAll(ctx, limit, offset)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "error get",
			"error":   err.Error(),
			"status":  false,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
		"status":  true,
		"data": map[string]interface{}{
			"result": data,
		},
	})
}

func (uc Controller) GetById(c *gin.Context) {
	//TODO implement me
	panic("implement me")
}

func (uc Controller) Update(c *gin.Context) {
	//TODO implement me
	panic("implement me")
}

func (uc Controller) Delete(c *gin.Context) {
	//TODO implement me
	panic("implement me")
}

func NewController(useCase *category.UseCase) *Controller {
	return &Controller{useCase: useCase}
}
