package category

import (
	"category/internal/entity"
	"category/internal/service/category"
	"context"
)

type Service interface {
	Create(ctx context.Context, data category.Create) error
	GetAll(ctx context.Context, limit, offset int) ([]entity.Category, error)
	GetById(ctx context.Context, id int) (entity.Category, error)
	Update(ctx context.Context, data category.Update) error
	Delete(ctx context.Context, id int) error
}

type UseCase struct {
	service Service
}

func NewUseCase(service Service) *UseCase {
	return &UseCase{service: service}
}

func (u UseCase) Create(ctx context.Context, data category.Create) error {
	return u.service.Create(ctx, data)
}

func (u UseCase) GetAll(ctx context.Context, limit, offset int) ([]entity.Category, error) {
	return u.service.GetAll(ctx, limit, offset)
}

func (u UseCase) GetById(ctx context.Context, id int) (entity.Category, error) {
	//TODO implement me
	panic("implement me")
}

func (u UseCase) Update(ctx context.Context, data category.Update) error {
	//TODO implement me
	panic("implement me")
}

func (u UseCase) Delete(ctx context.Context, id int) error {
	//TODO implement me
	panic("implement me")
}
