package news

import (
	"category/internal/entity"
	"category/internal/service/news"
	"context"
)

type Service interface {
	Create(ctx context.Context, data news.Create) error
	GetAll(ctx context.Context, limit, offset, categoryId int) (int, []entity.NewsPost, error)
	GetById(ctx context.Context, id int) (entity.NewsPost, error)
	Update(ctx context.Context, id int, data news.Update) error
	Delete(ctx context.Context, id int) error
}

type UseCase struct {
	service Service
}

func (u UseCase) Create(ctx context.Context, data news.Create) error {
	return u.service.Create(ctx, data)
}

func (u UseCase) GetAll(ctx context.Context, limit, offset, categoryId int) (int, []entity.NewsPost, error) {
	return u.service.GetAll(ctx, limit, offset, categoryId)
}

func (u UseCase) GetById(ctx context.Context, id int) (entity.NewsPost, error) {
	return u.service.GetById(ctx, id)
}

func (u UseCase) Update(ctx context.Context, id int, data news.Update) error {
	return u.service.Update(ctx, id, data)
}

func (u UseCase) Delete(ctx context.Context, id int) error {
	return u.service.Delete(ctx, id)
}

func NewUseCase(service Service) *UseCase {
	return &UseCase{service: service}
}
