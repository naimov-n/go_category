package news

import (
	"category/internal/entity"
	"context"
)

type Service struct {
	repo Repository
}

func NewService(repo Repository) *Service {
	return &Service{repo: repo}
}

func (s Service) Create(ctx context.Context, data Create) error {
	return s.repo.Create(ctx, data)
}

func (s Service) GetAll(ctx context.Context, limit, offset, categoryId int) (int, []entity.NewsPost, error) {

	count, data, err := s.repo.GetAll(ctx, limit, offset, categoryId)

	if err != nil {
		return 0, nil, err
	}
	var list []entity.NewsPost
	for _, item := range data {
		var news entity.NewsPost

		news.Id = item.Id
		news.CategoryId = item.CategoryId
		news.Title = item.Title
		news.Content = item.Content
		news.Status = item.Status
		list = append(list, news)

	}

	return count, list, nil
}

func (s Service) GetById(ctx context.Context, id int) (entity.NewsPost, error) {
	data, err := s.repo.GetById(ctx, id)

	if err != nil {
		return entity.NewsPost{}, err
	}

	var detail entity.NewsPost

	detail.Id = data.Id
	detail.Title = data.Title
	detail.Content = data.Content
	detail.Status = data.Status
	detail.CategoryId = data.CategoryId
	return detail, nil

}

func (s Service) Update(ctx context.Context, id int, data Update) error {
	return s.repo.Update(ctx, id, data)
}

func (s Service) Delete(ctx context.Context, id int) error {
	return s.repo.Delete(ctx, id)
}
