package news

type Create struct {
	Title      string
	Content    string
	CategoryId int
}

type Update struct {
	Title      string
	Content    string
	CategoryId int
}
