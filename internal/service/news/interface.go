package news

import (
	"category/internal/entity"
	"context"
)

type Repository interface {
	Create(ctx context.Context, data Create) error
	GetAll(ctx context.Context, limit, offset, categoryId int) (int, []entity.NewsPost, error)
	GetById(ctx context.Context, id int) (entity.NewsPost, error)
	Update(ctx context.Context, id int, data Update) error
	Delete(ctx context.Context, id int) error
}
