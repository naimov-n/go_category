package category

import (
	"category/internal/entity"
	"context"
)

type Repository interface {
	Create(ctx context.Context, data Create) error
	GetAll(ctx context.Context, limit, offset int) (int, []entity.Category, error)
	GetById(ctx context.Context, id int) (entity.Category, error)
	Update(ctx context.Context, data Update) error
	Delete(ctx context.Context, id int) error
}
