package category

import (
	"category/internal/entity"
	"context"
)

type Service struct {
	repo Repository
}

func (s Service) Create(ctx context.Context, data Create) error {
	return s.repo.Create(ctx, data)
}

func (s Service) GetAll(ctx context.Context, limit, offset int) (int, []entity.Category, error) {

	count, data, err := s.repo.GetAll(ctx, limit, offset)

	var detail []entity.Category
	if err != nil {
		return 0, nil, err
	}

	for _, item := range data {
		var category entity.Category

		category.Id = item.Id
		category.Title = item.Title
		category.Status = item.Status

		detail = append(detail, category)
	}

	return count, detail, nil
}

func (s Service) GetById(ctx context.Context, id int) (entity.Category, error) {
	//TODO implement me
	panic("implement me")
}

func (s Service) Update(ctx context.Context, data Update) error {
	//TODO implement me
	panic("implement me")
}

func (s Service) Delete(ctx context.Context, id int) error {
	//TODO implement me
	panic("implement me")
}

func NewService(repo Repository) *Service {
	return &Service{repo: repo}
}
