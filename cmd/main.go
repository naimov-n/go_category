package main

import (
	category4 "category/internal/controller/http/v1/category"
	news4 "category/internal/controller/http/v1/news"
	"category/internal/pkg/postgres"
	"category/internal/repository/postgres/category"

	"category/internal/repository/postgres/news"
	category2 "category/internal/service/category"
	news2 "category/internal/service/news"
	category3 "category/internal/usecase/category"
	news3 "category/internal/usecase/news"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"os"
)

func main() {
	r := gin.Default()

	if err := InitConfig(); err != nil {
		log.Fatalf("error init config: %s", err.Error())
	}

	db := postgres.NewPostgresConnect(postgres.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: goDotEnvVariable("PASSWORD_DB"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
	})

	newsRepository := news.NewRepository(db)
	newsService := news2.NewService(newsRepository)
	newsUsecase := news3.NewUseCase(newsService)
	newsController := news4.NewController(newsUsecase)

	// category

	csRepository := category.NewRepository(db)
	csService := category2.NewService(csRepository)
	csUsecase := category3.NewUseCase(csService)
	csController := category4.NewController(csUsecase)

	{
		api := r.Group("/api")

		categorys := api.Group("/category")
		{
			categorys.POST("/create", csController.Create)
			categorys.GET("/getAll", csController.GetAll)
			categorys.GET("/get/:id", csController.GetById)
			categorys.PUT("/update/:id", csController.Update)
			categorys.DELETE("/delete/:id", csController.Delete)
		}

		newsApi := api.Group("/news")
		{
			newsApi.POST("/create", newsController.Create)
			newsApi.GET("/getAll", newsController.GetAll)
			newsApi.GET("/get/:id", newsController.GetById)
			newsApi.PUT("/update/:id", newsController.Update)
			newsApi.DELETE("/delete/:id", newsController.Delete)
		}
	}

	r.Run(":6060")
}
func goDotEnvVariable(key string) string {

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}
func InitConfig() error {
	viper.AddConfigPath("internal/pkg/config")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
